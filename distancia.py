from math import *

def dec2grad (numero):
    dato=modf(numero)
    #grados
    grado=dato[1]
    #minutos
    x=dato[0]*60
    dato=modf(x)
    minuto=dato[1]
    #segundos
    segundo=dato[0]*60
    res=[grado,minuto,segundo]
    return res

print ("\tPROGRAMA PARA CALCULAR LA DISTANCIA MAS CORTA ENTRE DOS PUNTOS EN LA TIERRA")
print ("Para latitudes Sur ingresar el decimal con el signo -, de igual manera para longitudes Oeste\n\n")
print ("Coordenadas primer punto")
print ("Ingrese el valor de la latitud")
lat=float(input())
if (lat<0):
    l= 'S'
    lat=lat*-1
    z=1
else:
    l= 'N'
    z=2
print ("Ingrese el valor de la longitud")
long=float(input())
if (long<0):
    lo= 'O'
    long=long*-1
    w=1
else:
    lo= 'E'
    w=2
x=dec2grad(lat)
y=dec2grad(long)
print("Las coordenadas del primer punto son:")
print( str(x[0]) + "°"+ str(x[1])+ "´" + str(x[2]) + "´´"+ l )

print( str(y[0]) + "°" + str(y[1])+"´"+ str(y[2])+ "´´"+ lo  )
#---------------------
print ("Coordenadas segundo punto")
print ("Ingrese el valor de la latitud")
lat2=float(input())
if (lat2<0):
    l2= 'S'
    lat2=lat2*-1
    f=1
else:
    l2= 'N'
    f=2
print ("Ingrese el valor de la longitud")
long2=float(input())
if (long2<0):
    lo2= 'O'
    long2=long2*-1
    g=1
else:
    lo2= 'E'
    g=2
x2=dec2grad(lat2)
y2=dec2grad(long2)
print ("Las coordenadas segundo punto son:")
print( str(x2[0]) + "°" + str(x2[1]) + "´"+ str(x2[2])+ "´´" + l2)
print( str(y2[0]) + "°" + str(y2[1]) + "´" +str( y2[2] )+"´´" + lo2  )
#----------------
if(z==1):
    lat=lat*-1
else:
    lat=lat*1
if(f==1):
    lat2=lat2*-1
else:
    lat2=lat2*1

b=90-lat
a=90-lat2
#DIFERENCIA LONGS
if (w==1):
    long=long*-1
else:
    long=long*1

if (g==1):
    long2=long2*-1
else:
    long2=long2*1

if(long>=long2):
    cc=long-long2
else:
    cc=long2-long

#CALCULO DE DISTANCIA
m= radians(b)
n= radians(a)
o= radians(cc)

jj=((acos(cos(m)*cos(n)+sin(m)*sin(n)*cos(o))))
c=(jj *(180/pi)*111.32)

print(str(c)+"km")

#CUAL ESTA MAS CERCA DE MI CASA
#comparacion punto 1
casalat=4.711570
casalong=-74.130573
di=90-lat
dic=90-casalat
#DIFERENCIA LONGS
if (long>casalong):
    dif=long-casalong
else:
    dif=casalong-long
#CALCULO DE DISTANCIA pto1
md= radians(dic)
nd= radians(di)
od= radians(dif)

jjd=((acos(cos(md)*cos(nd)+sin(md)*sin(nd)*cos(od))))
distanpto1=(jjd *(180/pi)*111.32)
#comparacion punto 2
di2=90-lat2

#DIFERENCIA LONGS
if (long2>casalong):
    dif2=long2-casalong
else:
    dif2=casalong-long2
#CALCULO DE DISTANCIA pto1
md2= radians(dic)
nd2= radians(di2)
od2= radians(dif2)

jjd2=((acos(cos(md2)*cos(nd2)+sin(md2)*sin(nd2)*cos(od2))))
distanpto2=(jjd *(180/pi)*111.32)

if(distanpto1>distanpto2):
    print("El segundo punto se encuentra más cerca de mi casa, a una distancia de:")
    print(distanpto2)
else:
    print("El primer punto se encuentra más cerca de mi casa, a una distancia de:")
    print(str(distanpto1)+"km")